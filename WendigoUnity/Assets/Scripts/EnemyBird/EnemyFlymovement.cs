﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlymovement : MonoBehaviour
{
    private Spawn spawn;
    private PlayerMovement thePlayer;
    public float speed;
    public float playerRange;
    public Transform originPoint;
    private Vector2 dir = new Vector2(-1, 0);
    public float range;
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        thePlayer = FindObjectOfType<PlayerMovement>();
        spawn = FindObjectOfType<Spawn>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(originPoint.position, dir, range);
        if (hit == true)
        {
            if (hit.collider.CompareTag("backfly"))
            {

                //Flip();

                speed *= -1;

                // Destroy(gameObject);
            }
        }
        transform.position = Vector3.MoveTowards(transform.position, thePlayer.transform.position, speed * Time.deltaTime);
    }
    void FixedUpdate()
    {
        rb.velocity = new Vector2(speed, rb.velocity.y);
    }
    void Flip()
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
