﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Animator Anim;
    public float velocity = 10f;
    private HelloWord firstScript;
    private Sprite MySprite;
    private bool AbilityActivate = false;
    private float coldownTime = 5f;
    private float timeAbility = 3f;
    private float contador;
    private bool BeGigant = false;


    private GigantStates gigantCurrentState=GigantStates.Normal;

    //Capturamos el momento en el que se activa la habilidad
    private float activatedGigantTime = 0f;


    private enum GigantStates
    {
        Normal,
        Growing,
        Gigant,
        BeLittle,
        Coldown
    }
    
    [Header("Gigant Hability variables")]
    [Tooltip("Scale Number es el numero de veces que se agranda el personaje")]
    public float scaleNumber = 5f;

    [Tooltip("Tiempo de crecimiento en segundos")]
    public float growingTime = 1f;

    [Tooltip("Tiempo que permanece en modo Gigante en segundos")]
    public float gigantTime = 5f;

    [Tooltip("Tiempo de decrecimiento en segundos")]
    public float beLittleTime = 1f;

    [Tooltip("Tiempo que dura el cooldown en segundos")]
    public float coolDownTime = 3f;

    // Start is called before the first frame update
    void Start()
    {
        Anim = this.gameObject.GetComponent<Animator>();
        firstScript = new HelloWord();
        MySprite = this.gameObject.GetComponent<Sprite>();
        gigantCurrentState = GigantStates.Normal;

    }

    // Update is called once per frame
    void Update()
    {
        float Weight = firstScript.characterWeight;

        var displacament = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        transform.position += displacament * velocity / Weight;

        
        Anim.SetFloat("Horizontal", displacament.x);
        Anim.SetFloat("Vertical", displacament.y);


        
        /*

        if (Input.GetKeyDown(KeyCode.Space) && AbilityActivate == false){
            contador = Time.time;
            AbilityActivate = true;
            BeGigant = true;
        }
        if (AbilityActivate)
        {
            if (AbilityActivate && BeGigant){
                this.transform.localScale = new Vector3(this.transform.localScale.x + 0.5f, this.transform.localScale.y + 0.5f, 0);
                Debug.Log("Es grande");
                BeGigant = false;
            }
            if (Time.time - contador > timeAbility)
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - 0.5f, this.transform.localScale.y - 0.5f, 0);
                Debug.Log("Es pequeño");

                AbilityActivate = false;
            }
            if (!BeGigant&&!AbilityActivate){
                if (Time.time - contador < coldownTime){
                }
            }
        }
    }
    */
        if (Input.GetKeyDown(KeyCode.Space) && gigantCurrentState == GigantStates.Normal){
            gigantCurrentState = GigantStates.Growing;
        }
        gigantHabilityUpdate();
    }

    void gigantHabilityUpdate()
    {
        switch (gigantCurrentState)
        {
            case GigantStates.Normal:
                break;
            case GigantStates.Growing:
                doGrowing();
                break;
            case GigantStates.Gigant:
                doGigant();
                break;
            case GigantStates.BeLittle:
                doBeLittle();
                break;
            case GigantStates.Coldown:
                doColdown();
                break;
            default:
                gigantCurrentState = GigantStates.Normal;
                break;
        }
        if(gigantCurrentState != 0)
        {
            Debug.Log("Rest cooldown time: " + ((activatedGigantTime +gigantTime + coldownTime)-Time.time).ToString());
        }
    }

    private void doGrowing()
    {
        activatedGigantTime = Time.time;
        transform.localScale *= scaleNumber;
        //Para hacerlo grande
        nextGigantState();

    }

    void nextGigantState()
    {
        if(gigantCurrentState == GigantStates.Coldown)
        {
            gigantCurrentState = 0;
        }else
        {
            gigantCurrentState++;
        }
    }

    private void doColdown()
    {
        if (Time.time > coldownTime + activatedGigantTime + gigantTime)
        {
            nextGigantState();
        }
    }

    private void doBeLittle()
    {
     // transform.localScale = new Vector3(1f, 1f, 1f);
        transform.localScale /= scaleNumber;
        nextGigantState();
    }

    private void doGigant()
    {
        if (Time.time > activatedGigantTime + gigantTime){
            nextGigantState();
        }
    }
 
    void OnTriggerEnter2D(Collider2D spike)
    { 
        if (spike.tag=="enemy" || spike.tag=="pinchos")
    {
     Destroy(gameObject);   
    }
    }
}
