﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStates {
    mainMenu,
    playing,
    gameWin,
    gameDead
}

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public GameStates gameState;
    private bool mainMenuDone = false;
    public int life =3;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
            Debug.Log("GameManager: I'm alive");
        }
        else
        {
            Destroy(this);
            Debug.Log("GameManager: I have a brother, just live one");

        }
    }

    // Start is called before the first frame update
    void Start()
    {
       if (!mainMenuDone)
        {
            /*
            this.gameState = GameStates.tutorial;
            doTutorial();
            */
            this.setGameState(GameStates.mainMenu);
        }
        else
        {
            this.setGameState(GameStates.playing);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (life <= 0)
        {
            this.setGameState(GameStates.gameDead);
        }
    }

    public GameStates getGameState()
    {
        return this.gameState;
    }

    public void setGameState(GameStates _gs)
    {
        this.gameState = _gs;
        updateGameState();
    }

    void updateGameState()
    {

        switch (this.gameState)
        {
            case GameStates.mainMenu:
                doMainMenu();
                break;
            case GameStates.playing:
                doPlaying();
                break;
            case GameStates.gameDead:
                doGameDead();
                break;
            case GameStates.gameWin:
                //doGameWin();
                break;

            default:
                //doPlaying();
                break;
        }
    }

    void doMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    void doPlaying()
    {

    }
    void doGameDead()
    {
        SceneManager.LoadScene("Restart");
        life = 3;
    }

    void OnTriggerEnter2D(Collider2D player)
    {
        if(player.tag == "Player")
        {
            life = 0;
        }
    }
}
