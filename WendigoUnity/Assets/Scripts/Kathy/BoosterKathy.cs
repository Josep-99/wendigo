﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoosterKathy : MonoBehaviour
{
    private PlayerMovement playerMovement = null;
    private Rigidbody2D _rb;
	private bool active;
    private bool doubleJump = false;
    public float boostTimeActive = 5f;
    public float boostTimer = 1f;
    GameObject tempObj;
    public BoostState boostState;
    private Animator Anim;
    private bool isDoubleJumping;

    public enum BoostState {
        Ready,
        Boosting
    }

    // Start is called before the first frame update
    void Start()
    {
        Anim = this.gameObject.GetComponent<Animator>();
        tempObj = GameObject.Find("PlayerKathy");
        playerMovement = tempObj.GetComponent<PlayerMovement>();
        _rb = playerMovement.GetComponent<Rigidbody2D>();
	    active = false;
        isDoubleJumping = false;
        }

    // Update is called once per frame
    void Update()
    {
        if (active) {
            if (Input.GetKeyDown(KeyCode.Space) && playerMovement.canDoubleJump) {
                doubleJump = true;
            }
        }
        
    }

     void FixedUpdate() 
    {
        switch (boostState) {
         
            case BoostState.Ready:
                if (active) {   
                    boostState = BoostState.Boosting;
                }
                break;

            case BoostState.Boosting:
                Anim.SetBool("isDoubleJumping", true);
                CanDoubleJumping();
                boostTimer += Time.deltaTime;
                if(boostTimer >= boostTimeActive)
                {
                    active = false;
                    boostTimer = 1;
                    boostState = BoostState.Ready;
                }
                if(playerMovement.isOnGround()) {
                    Anim.SetBool("isDoubleJumping", false);
                }
                break;
        }
    }

void CanDoubleJumping() {
    //salto
        if (doubleJump) 
        {
            _rb.AddForce(Vector2.up * (playerMovement.jumpForce / 2), ForceMode2D.Impulse);
            doubleJump = false;
            playerMovement.canDoubleJump = false;
            isDoubleJumping = true;
        }       
}

void OnTriggerEnter2D(Collider2D spike) {
    if (spike.tag == "MushroomBoost") { 
        Destroy(spike.gameObject);
        active = true;
    }
}
}
