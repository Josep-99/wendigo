﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBasicInfo : MonoBehaviour
{
    public string name = "Kathy";
    public string surname = "Miller";
    public int age = 15;
    public float weight = 51.5f;
    public float height = 159f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
