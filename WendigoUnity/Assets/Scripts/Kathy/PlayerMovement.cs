﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public PlayerBasicInfo basicInfo;
    public Rigidbody2D _rb;
    private SpecialAbilityKathy dash;
    private Animator Anim;

    // MOVEMENT
    public float velocity = 10f;
    private bool facingRight;
    // ABILITY
    private float cooldownTime = 5f;
    // private float timeAbility (?) es solo un momento
    // JUMP
    public bool jump = false; //saltando
    public bool canDoubleJump = false; //util para script boosterKathy
    public float jumpForce = 4f;
    public float fallJumpForce = -30f;
    private int vidas = 3;
    public float rcDistance = 1f;
    public bool isDashing;
    public LayerMask layerGround;

    private AudioSource _as;
    public AudioClip[] audios;    

//animation
    public bool isJumping;
    public bool isFalling;
    private float currentPosition;
    private float previousPosition = 0f;

    // other used 
    // private Sprite MySprite; --> para cambiar la imagen supongo
    
    // Start is called before the first frame update
    void Start()
    {
        Anim = this.gameObject.GetComponent<Animator>();
        _rb = this.gameObject.GetComponent<Rigidbody2D>();
        basicInfo = new PlayerBasicInfo();
        _as = this.gameObject.GetComponent<AudioSource>();
        dash = this.gameObject.GetComponent<SpecialAbilityKathy>();
    
        isJumping = false;
        isFalling = false;
        facingRight = true;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(dash.isDashing);
        Debug.Log(GameManager._instance.life);
        var displacement = new Vector3();

        float weight = basicInfo.weight;
        displacement = new Vector3(Input.GetAxis("Horizontal"), 0, 0);

        // horizontal movement 
        _rb.transform.Translate(displacement * velocity / weight);

        // que haga ruido cuando camine
        if (displacement.x != 0)
        {
            if (!_as.isPlaying)
            {
                _as.Play();
            }
        }
        else
        {
            _as.Stop();
        }


         if (Input.GetAxis("Horizontal") < 0)
        {
            _rb.velocity = new Vector2(-1, _rb.velocity.y);
            transform.localScale= new Vector2(-1,1);
        }
         if (Input.GetAxis("Horizontal")>0)  
        {
            _rb.velocity = new Vector2(1, _rb.velocity.y);
            transform.localScale = new Vector2(1, 1);
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            invulnerable();
        }
        if (Input.GetKeyDown(KeyCode.Space) && isOnGround()) 
        {
            jump = true;
        }   
        if (_rb.velocity.y < 0) 
        {
           this._rb.AddForce (Vector3.up * fallJumpForce);
            //_rb.velocity += Vector2.up * Physics2D.gravity.y * (fallJumpForce - 1) * Time.deltaTime;
        }  
        if (jump) 
        {
            canDoubleJump = false;
        }   

        //animaciones correr
        if (displacement.x == 0) {
            Anim.SetBool("isRunning", false);
        } else {
            Anim.SetBool("isRunning", true);
        }
    }

    void FixedUpdate() 
    {
        //salto
        if (jump && isOnGround()) 
        {
            _rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            jump = false;
            canDoubleJump = true;
        }        
    }

    void LateUpdate() { //ANIMACIONES

        //animaciones girar
         

        //animacion saltar
        if (!isOnGround()) {
            isJumping = true;
        }
        if (isOnGround()) {
            //Anim.SetBool("isFalling", false);
            isJumping = false;
            Anim.SetBool("isJumping", false);
        }

        if (isJumping) {
            Anim.SetBool("isRunning", false);
            Anim.SetBool("isJumping", true);
        } 
        if (!isJumping) {
            Anim.SetBool("isJumping", false);
        }
    }

    // mira si el personaje esta en contacto con la capa Ground
    public bool isOnGround()
    {
        return Physics2D.Raycast(_rb.transform.position, Vector2.down, rcDistance, layerGround);
    }

    IEnumerable invulnerable()
    {
        GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(5f);
        GetComponent<Collider2D>().enabled = true;
    }

    private void Flip(float horizontal) {

        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight) {
           facingRight = !facingRight;
           
            Transform bone = transform.Find("Bones/torso");
            
            if (bone.transform.rotation.y == 180) {
                bone.rotation = Quaternion.Euler(0, 0, 93);
                Debug.Log("eq");
            } else {
                bone.rotation = Quaternion.Euler(0, 180, 93);
            }
        }

    }

    // trigger
    void OnTriggerEnter2D(Collider2D spike)
    {
        if (!dash.isDashing)
        {
            if (spike.tag == "enemy" || spike.tag == "spike")
            {
                if (GameManager._instance.life <= 0)
                {
                    Debug.Log("Muerto");
                }
                else
                {
                    Debug.Log(vidas - 1);
                    GameManager._instance.life -= 1;
                }
            }
        }
    }
}
