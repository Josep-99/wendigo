﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContadorColeccionable : MonoBehaviour
{
    public Text puntos;
    private int contador;

    // Start is called before the first frame update
    void Start()
    {
        contador = 0;
    }

    // Update is called once per frame
    
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D spike)
    { 
        if (spike.tag == "Coleccionable") { 
            Destroy(spike.gameObject);
            contador = contador + 1;
            puntos.text = "Coleccionables: " + contador;
        }
    }
}
