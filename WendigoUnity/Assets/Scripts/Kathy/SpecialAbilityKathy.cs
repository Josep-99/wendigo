﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialAbilityKathy : MonoBehaviour
{
    private Rigidbody2D _rb;
    private GameObject tempObj;
    private PlayerMovement playerMovement;

    //dash
    public DashState dashState;
    public float dashTimer = 2f;
    public float maxDash = 5f;
    public float normalVelocity;
    public float dashVelocity = 50f;
    public bool isDashing = false;
    public Vector2 savedVelocity;

    public enum DashState {
        Ready,
        Dashing,
        Cooldown
    }

    // Start is called before the first frame update
    void Start()
    {
        tempObj = GameObject.Find("PlayerKathy");
        playerMovement = tempObj.GetComponent<PlayerMovement>();
        _rb = playerMovement.GetComponent<Rigidbody2D>();
        normalVelocity = playerMovement.velocity;
    }

    // Update is called once per frame
    void Update() {
        
    }
    
    void FixedUpdate() {
        switch (dashState) {
         
            case DashState.Ready:
                var isDashKeyDown = Input.GetKeyDown (KeyCode.LeftShift);
                if(isDashKeyDown) {
                    //savedVelocity = _rb.velocity;
                    //_rb.velocity =  new Vector3(_rb.velocity.x * dashVelocity, _rb.velocity.y, 0);
                    playerMovement.velocity = dashVelocity;
                    dashState = DashState.Dashing;
                }
                break;

            case DashState.Dashing:
                isDashing = true;
                dashTimer += Time.deltaTime * 50;
                if(dashTimer >= maxDash)
                {
                    dashTimer = maxDash;
                    _rb.velocity = savedVelocity;
                    dashState = DashState.Cooldown;
                }
                break;

            case DashState.Cooldown:
                isDashing = false;
                dashTimer -= Time.deltaTime;
                playerMovement.velocity = normalVelocity;
                if(dashTimer <= 0)
                {
                    dashTimer = 0;
                    dashState = DashState.Ready;
                }
                break;
        }
    }
}
